package com.relocro.spring.service;

import com.relocro.spring.model.Cliente;

import java.util.List;

public interface IClienteService {

    public List<Cliente> findAll();

    public void save(Cliente cliente);

    public Cliente findById(Long id);

    public void delete(Cliente cliente);
}
