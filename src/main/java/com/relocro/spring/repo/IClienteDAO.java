package com.relocro.spring.repo;
import com.relocro.spring.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface IClienteDAO extends CrudRepository<Cliente,Long> {



}
